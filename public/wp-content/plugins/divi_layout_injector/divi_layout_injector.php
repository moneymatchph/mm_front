<?php

/*
 * Plugin Name: Divi/Extra Layout Injector
 * Plugin URI:  http://www.sean-barton.co.uk
 * Description: A simple plugin to edit the global layouts in Divi. Top and tail any page in a number of locations sitewide. Override based on a number of settings.
 * Author:      Sean Barton - Tortoise IT
 * Version:     2.5
 * Author URI:  http://www.sean-barton.co.uk
 *
 * Changelog:
 *
 * < V1.8
 * - Initial Versions
 *
 * V1.8
 * - Fixed conflict with the HTML5 'footer' tag
 * 
 * V1.9
 * - Fixed bug introduced in V1.8
 * 
 * V2.0
 * - Added support for Polylang. Choose different layouts to show per language!
 * 
 * V2.1 - 30/1/17
 * - Fixed bug whereby the footer id has additional quotes
 * - Added licensing and auto update functionality
 * 
 * V2.2 - 10/2/17
 * - Removed divi layout injector overrides box for post type et_pb_layout, acf-field-group for neatness
 * - Slight admin page change to decrease need to scroll
 * 
 * V2.3 - 22/2/17
 * - Added Widget to use a Divi Builder layout
 * 
 * V2.4 - 25/4/17
 * - Changed post content layout to work even when page builder is not used.
 *
 * V2.5 - 15/6/17
 * - Added NEW "post menu" injection point which sits below the menu and remains sticky with the scroll. Ideal for opt ins and additional menus/buttons etc..
 * - Fixed pre-header repeating where a page had multiple <header> html tags
 * - Added 'hide on scroll' option to post-menu injection point
 * 
 */

 define('SB_DIVI_FE_VERSION', '2.5');
 
 require_once('includes/emp-licensing.php');
 
    add_action('plugins_loaded', 'sb_divi_fe_init');
    
    function sb_divi_fe_init() {
    
        add_action('get_header', 'sb_divi_fe_record_start', 1, 1);
        add_action('wp_footer', 'sb_divi_fe_footer_end');
        add_action('admin_menu', 'sb_divi_fe_submenu');
        add_action('template_redirect', 'sb_divi_fe_404');
        add_action("save_post", "sb_divi_fe_meta_box_save", 10, 3);
        add_action("add_meta_boxes", "sb_divi_fe_meta_box");
        add_action('wp_enqueue_scripts', 'sb_divi_fe_enqueue', 9999);
        add_action('widgets_init', 'sb_divi_fe_widget');
        
        add_filter('the_content', 'sb_divi_fe_content');
     
        add_shortcode('sb_divi_date', 'sb_divi_date');
        add_shortcode('sb_divi_blogname', 'sb_divi_blogname');
        
    }
    
    function sb_divi_fe_widget() {
        register_widget( 'DLI_Widget' );
    }
    
    function sb_divi_fe_enqueue() {
				//wp_enqueue_script('jquery');
				wp_enqueue_style('sb_divi_fe_custom_css', plugins_url( '/style.css', __FILE__ ));
    }
 
    function sb_divi_date() {
        return date('Y');
    }
 
    function sb_divi_blogname() {
        return get_bloginfo('name');
    }
 
    function sb_divi_fe_record_start() {
        ob_start();
    }
    
    function sb_divi_fe_404() {
        if (is_404()) {
            $type_selection = get_option('sb_divi_fe_404_type', 'layout');
            
            if ($type_selection == 'layout') {
                if ($layout_404 = get_option('sb_divi_fe_404')) {
                    get_header();
                    echo wpautop(do_shortcode('[et_pb_section global_module="' . $layout_404 . '"][/et_pb_section]'));
                    get_footer();
                    exit();
                }
            } else if ($type_selection == 'page') {
                if ($layout_404 = get_option('sb_divi_fe_404_page_id')) {
                    if (get_post_status($layout_404) == 'publish') {
                        wp_redirect(get_permalink($layout_404));
                        die;
                    }
                }
            }
        }
    }
    
    function sb_divi_fe_can_show($show_when) {
        
        $show_when = get_option('sb_divi_fe_applicable_' . $show_when);
        
        //echo '<pre>';
        //print_r($show_when);
        //echo '</pre>';
        
        if (!sb_divi_fe_check_show_on_blank()) {
            return false;
        }
        
        //if (is_archive()) {
            //return false;
        //}
        
        if (!$show_when) {
            return true; //show everywhere except archive pages
        }
        
        foreach (array_keys($show_when) as $when) {
            //echo $when . '<br />';
            
            //if we're post type based then use this.
            if (get_post_type() == $when) {
                return true;
            }  
            
            switch ($when) {
                case 'home':
                    if (is_home()) {
                        return true;
                    }
                    break;
                case 'front_page':
                    if (is_front_page()) {
                        return true;
                    }
                    break;
                case 'page':
                    if (is_page()) {
                        return true;
                    }
                    break;
                case 'single':
                    if (is_single()) {
                        return true;
                    }
                    break;
                case '404':
                    if (is_404()) {
                        return true;
                    }
                    break;
            }
            
        }
        
        return false;
    }
    
    function sb_divi_fe_meta_box_content() {
        
        echo '<p>Use these settings to override the default settings of the Divi Layout Injector plugin. Each location below can be independently left as default, turned off or overridden.</p>';

        $locations = array(
            'sb_divi_fe_pre-header'=>'Pre Header'
            , 'sb_divi_fe_post-menu'=>'Post Menu'
            , 'sb_divi_fe_pre-content'=>'Pre Content'
            , 'sb_divi_fe_post-content'=>'Post Content'
            , 'sb_divi_fe_pre-footer'=>'Pre Footer'
        );
        
        $overrides = get_post_meta(get_the_ID(), "sb_divi_fe_layout_overrides", true);
        
        $layout_query = array(
            'post_type'=>'et_pb_layout'
            , 'posts_per_page'=>-1
            , 'meta_query' => array(
                array(
                    'key' => '_et_pb_predefined_layout',
                    'compare' => 'NOT EXISTS',
                ),
            )
        );
        
        if ($layouts = get_posts($layout_query)) {
            foreach ($locations as $key=>$value) {
                if (!isset($overrides[$key])) {
                    $overrides[$key] = 0;
                }
                
                echo '<p><label>' . $value . '</label><br />';
                echo '<select style="max-width: 175px;" name="sb_divi_fe_layout_overrides[' . $key . ']">';
                
                echo '<option value="0" ' . selected($overrides[$key], 0, false) . '>-- Default --</option>';
                echo '<option value="-1" ' . selected($overrides[$key], -1, false) . '>-- Disable --</option>';
                
                foreach ($layouts as $layout) {
                    echo '<option  ' . selected($overrides[$key], $layout->ID, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                }
                
                echo '</select></p>';
            }
        }        
        
    }
    
    function sb_divi_fe_meta_box_save($post_id, $post, $update) {
        
        if (!current_user_can("edit_post", $post_id))
            return $post_id;
    
        if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
            return $post_id;
    
        if (isset($_POST['sb_divi_fe_layout_overrides'])) {
            update_post_meta($post_id, 'sb_divi_fe_layout_overrides', $_POST['sb_divi_fe_layout_overrides']);
        }   
        
    }
    
    function sb_divi_fe_meta_box() {
        $excludes = array('et_pb_layout', 'acf-field-group');
        
        if ($pt = get_post_types()) {
            foreach ($pt as $t) {
                if (in_array($t, $excludes)) {
                    continue;
                }
                add_meta_box('sb_divi_fe_meta_box', 'Divi Layout Injector', 'sb_divi_fe_meta_box_content', $t, 'side');
            }
        }
    }
    
    function sb_divi_fe_get_layout($key) {
        $root_key = $key;
        $key = 'sb_divi_fe_' . $key;
        
        $return = get_option($key);
        
        if (!sb_divi_fe_can_show($root_key)) {
            $return = false;
        }
        
        if (function_exists('PLL')) { //polylang
            //$list = pll_languages_list();
            $default = pll_default_language();
            $current = pll_current_language();
            
            if ($current != $default) {
                $lang_settings = get_option('sb_divi_fe_lang', array());
                
                if (isset($lang_settings[$current][$root_key]) && $lang_settings[$current][$root_key]) {
                    $return = $lang_settings[$current][$root_key];
                }
            }
        }
        
        if (is_singular()) {
            if ($overrides = get_post_meta(get_the_ID(), 'sb_divi_fe_layout_overrides', true)) {
                if ($overrides[$key] > 0) { //0 is default, anything else is a layout
                    $return = $overrides[$key];
                } else if ($overrides[$key] == -1) { //override disable this
                    $return = false;
                }
            }
        }
								
        return $return;
    }
    
    function sb_divi_fe_content($content) {
        
        $is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
        
        if ($is_page_builder_used) {
            
            if (is_page_template( 'page-template-blank.php' )) {
                if ($pre_header_layout = sb_divi_fe_get_layout('pre-header')) {
                    $section = do_shortcode('[et_pb_section global_module="' . $pre_header_layout . '"][/et_pb_section]');
                    $content = $section . $content; //prepend
                }
            }
        }
				
				if (!is_archive() && !is_search()) {
            if ($post_content_layout = sb_divi_fe_get_layout('post-content')) {
                if ($section = do_shortcode('[et_pb_section global_module="' . $post_content_layout . '"][/et_pb_section]')) {
                    $content .= $section; //append
                }
            }
				}
        
        return $content;
    }
    
    function sb_divi_fe_footer_end() {
       $footer = ob_get_clean();
       
        if ($pre_header_layout = sb_divi_fe_get_layout('pre-header')) {
            if ($section = sb_divi_fe_add_class(do_shortcode('[et_pb_section global_module="' . $pre_header_layout . '"][/et_pb_section]'), 'pre-header')) {
                $footer = str_replace("<header id=\"main-header\"", $section . '<header id="main-header"', $footer); //crude but replaces the header tag with the section and then reinstates the tag
            }
        }
       
        if ($post_menu_layout = sb_divi_fe_get_layout('post-menu')) {
										if ($header_pos = strpos($footer, '</header>')) {
													$classes = array('post-menu');
													
													if (get_option('sb_divi_fe_post-menu_hide_scroll')) {
														$classes[] = 'post-menu-hide-scroll';
													}
											
													if ($section = sb_divi_fe_add_class(do_shortcode('[et_pb_section global_module="' . $post_menu_layout . '"][/et_pb_section]'), $classes)) {
																	$footer = substr_replace($footer, $section, $header_pos, 0); //just before the </header> tag
													}
										}
        }

        if ($pre_content_layout = sb_divi_fe_get_layout('pre-content')) {
										if ($header_pos = strpos($footer, '</header>')) {
													if ($section = sb_divi_fe_add_class(do_shortcode('[et_pb_section global_module="' . $pre_content_layout . '"][/et_pb_section]'), 'pre-content')) {
																	$footer = substr_replace($footer, $section, ($header_pos+9), 0);
													}
										}
        }
        
        if ($hide_header = get_option('sb_divi_fe_hide_header')) {
            $footer = preg_replace("/<header[^<>]*>([\s\S]*?)<\/header>/i", '', $footer); //remove the header including the tag
        }
        
        if ($footer_credit = trim(get_option('sb_divi_fe_content'))) {
            if (sb_divi_fe_check_show_on_blank()) {
                if (!get_option('sb_divi_fe_adv_content')) {
                    $footer_credit = '<p id="footer-info">' . $footer_credit . '</p>';
                }
                
                $footer = preg_replace("/<p.*?id=\"footer-info\">.*?<\/p>/i", '<span class="sb_divi_fe">' . do_shortcode($footer_credit) . '</span>', $footer);
            }
        }
        
        //<footer id="main-footer">
        if ($pre_footer_layout = sb_divi_fe_get_layout('pre-footer')) {
            $section = sb_divi_fe_add_class(do_shortcode('[et_pb_section global_module="' . $pre_footer_layout . '"][/et_pb_section]'), 'pre-footer');
            $footer = str_replace("<footer id=\"main-footer\"", $section . '<footer id="main-footer"', $footer); //crude but replaces the footer tag with the section and then reinstates the tag
        }
       
       echo $footer;
    }
				
				function sb_divi_fe_add_class($html, $class) {
					if (is_array($class)) {
						foreach($class as &$cl) {
							$cl = 'sb_dli_' . str_replace('-', '_', $cl);
						}
						$class = implode(' ', $class);
					} else {
						$class = 'sb_dli_' . str_replace('-', '_', $class);
					}
					
					$html = str_replace('class="et_pb_section', 'class="' . $class . ' et_pb_section ', $html);
					
					return $html;
				}
    
    function sb_divi_fe_check_show_on_blank() {
        $return = true;
        
        if (!get_option('sb_divi_fe_inc_blank') && is_page_template('page-template-blank.php')) {
            $return = false;
        }
        
        return $return;
    }
    
    function sb_divi_fe_submenu() {
        add_submenu_page(
            'options-general.php',
            'Divi Layout Injector',
            'Divi Layout Injector',
            'manage_options',
            'sb_divi_fe',
            'sb_divi_fe_submenu_cb' );
    }
    
    function sb_divi_fe_box_start($title, $width=false, $float='left') {
        return '<div class="postbox" style="' . ($width ? 'float: ' . $float . '; margin-bottom: 20px; width: ' . $width:'clear: both;') . '">
                    <h2 class="hndle">' . $title . '</h2>
                    <div class="inside" style="clear: both;">';
    }
    
    function sb_divi_fe_box_end() {
        return '    <div style="display: table; clear: both;">&nbsp;</div></div>
                </div>';
    }
     
    function sb_divi_fe_submenu_cb() {
        
        $options = array(
            'front_page'=>'Homepage'
            , 'home'=>'Blog home'
            , 'page'=>'Pages'
            , 'single'=>'Posts'
            , '404'=>'404 Page'
        );
        
        $args = array(
            'public'   => true,
            '_builtin' => false
        );
        
        $types = get_post_types($args, 'objects');
        
        foreach ($types as $type=>$obj) {
            $options[$type] = $obj->labels->name;
        }
        
        $languages = false;
        $default_lang = false;
        
        if (function_exists('PLL')) { //polylang
            $languages = PLL()->model->get_languages_list();
            $default_lang = pll_default_language();
        }
        
        echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
        echo '<h2>Divi Layout Injector - V' . SB_DIVI_FE_VERSION . '</h2>';
                
        echo '<div id="poststuff">';
        
        echo '<div id="post-body" class="metabox-holder columns-2">';
        
        if (isset($_POST['sb_divi_fe_edit_submit'])) {
                update_option('sb_divi_fe_lang', $_POST['sb_divi_fe_lang']);
                
                update_option('sb_divi_fe_content', stripslashes($_POST['sb_divi_fe_editor']));
                update_option('sb_divi_fe_adv_content', $_POST['sb_divi_fe_adv_content']);
                update_option('sb_divi_fe_inc_blank', $_POST['sb_divi_fe_inc_blank']);
                update_option('sb_divi_fe_hide_header', $_POST['sb_divi_fe_hide_header']);
                update_option('sb_divi_fe_404', $_POST['sb_divi_fe_404']);
                update_option('sb_divi_fe_404_type', $_POST['sb_divi_fe_404_type']);
                update_option('sb_divi_fe_404_page_id', $_POST['sb_divi_fe_404_page_id']);
                update_option('sb_divi_fe_pre-footer', $_POST['sb_divi_fe_pre-footer']);
                update_option('sb_divi_fe_pre-header', $_POST['sb_divi_fe_pre-header']);
                update_option('sb_divi_fe_post-menu', $_POST['sb_divi_fe_post-menu']);
                update_option('sb_divi_fe_post-menu_hide_scroll', $_POST['sb_divi_fe_post-menu_hide_scroll']);
                update_option('sb_divi_fe_pre-content', $_POST['sb_divi_fe_pre-content']);
                update_option('sb_divi_fe_post-content', $_POST['sb_divi_fe_post-content']);
                update_option('sb_divi_fe_applicable_post-menu', $_POST['sb_divi_fe_applicable_post-menu']);
                update_option('sb_divi_fe_applicable_post-content', $_POST['sb_divi_fe_applicable_post-content']);
                update_option('sb_divi_fe_applicable_pre-content', $_POST['sb_divi_fe_applicable_pre-content']);
                update_option('sb_divi_fe_applicable_pre-header', $_POST['sb_divi_fe_applicable_pre-header']);
                
                echo '<div id="message" class="updated fade"><p>Layouts edited successfully</p></div>';
        }
        
        $dli_layout = get_option('sb_divi_fe_lang');
        $content = get_option('sb_divi_fe_content');
        $editor_id = 'sb_divi_fe_editor';
        
                
        $layout_query = array(
            'post_type'=>'et_pb_layout'
            , 'posts_per_page'=>-1
            , 'meta_query' => array(
                array(
                    'key' => '_et_pb_predefined_layout',
                    'compare' => 'NOT EXISTS',
                ),
            )
        );
        
        $layouts = get_posts($layout_query);
        
        echo '<p>This plugin allows you to edit the layouts of your Divi site without having to edit any core files. See each section below for more information</p>';
        
        echo '<form method="POST">';
        
        sb_divi_fe_license_page();
        
        //echo '<p id="submit"><input type="submit" name="sb_divi_fe_edit_submit" class="button-primary" value="Save Settings" /></p>';
        
        echo '<div style="clear: both;">';
            
        echo sb_divi_fe_box_start('General Settings', '49%');
        
        echo '<p>
            <label><input type="checkbox" name="sb_divi_fe_inc_blank" ' . checked(1, get_option('sb_divi_fe_inc_blank', 0), false) . ' value="1" /> Show on "blank" template?</label>
            <br /><small>Divi has a "blank" template which omits the header and footer. This plugin will not inject into those pages either. Check this box to add it to those pages.</small>
        </p>';
        
        echo '<p>
            <label><input type="checkbox" name="sb_divi_fe_hide_header" ' . checked(1, get_option('sb_divi_fe_hide_header', 0), false) . ' value="1" /> Hide default header entirely?</label>
            <br /><small>If you want to remove the header entirely in favour of one powered by injector then check this box.</small>
        </p>';
        
        echo sb_divi_fe_box_end();
        
        echo sb_divi_fe_box_start('404 Layout', '49%', 'right');
        
        if ($layouts) {
            echo '<p><small>Pick a display type.. layout is built using the Divi Library per the rest of the parts of this page. It\'s a nice approach but for more complex layouts has some issues. The alternative is a page based 404 whereby whereby a redirect takes any 404 requests to a page of your choice. This consistently works but does change the URL which may not be preferable in some cases.</small></p>';
            
            $type_selection = get_option('sb_divi_fe_404_type', 'layout');
            
            echo '<span style="display: inline-block; width: 100px;"><label><input type="radio" class="sb_divi_fe_404_type_layout" name="sb_divi_fe_404_type" ' . checked(1, (@$type_selection == 'layout'), false) . ' value="layout" /> ' . $value . '</label> Layout</span>';
            echo '<select style="width: 250px;" name="sb_divi_fe_404" onchange="jQuery(\'.sb_divi_fe_404_type_layout\').click();">';
            
                $layout_404 = get_option('sb_divi_fe_404');
                echo '<option value="">-- None --</option>';
                
                foreach ($layouts as $layout) {
                    echo '<option ' . selected($layout->ID, $layout_404, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                }
    
            echo '</select>';
            
        } else {
            echo '<p>Layout based 404 pages will be available when a layout has been added to the library.</p>';
        }
        
        echo '<br />';
        
        echo '<span style="display: inline-block; width: 100px;"><label><input type="radio" class="sb_divi_fe_404_type_page" name="sb_divi_fe_404_type" ' . checked(1, (@$type_selection == 'page'), false) . ' value="page" /> ' . $value . '</label> Page</span>';
        echo '<select style="width: 250px;" name="sb_divi_fe_404_page_id" onchange="jQuery(\'.sb_divi_fe_404_type_page\').click();">';
        
            $layout_404_page_id = get_option('sb_divi_fe_404_page_id');
            echo '<option value="">-- None --</option>';
            
            $pages = get_posts(array('post_type'=>'page', 'posts_per_page'=>-1, 'orderby'=>'name', 'order'=>'asc'));
            
            foreach ($pages as $layout) {
                echo '<option ' . selected($layout->ID, $layout_404_page_id, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
            }

        echo '</select>';
        
        echo sb_divi_fe_box_end();
        
        echo '</div>';
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        echo sb_divi_fe_box_start('Layouts');
        
        echo '<a href="' . plugin_dir_url( __FILE__ ) . '/images/areas.jpg" target="_blank"><img class="" style="float: right; padding: 0 20px; display: inline-block; max-width: 300px;" src="' . plugin_dir_url( __FILE__ ) . '/images/areas.jpg" /></a>
        <p>A layout can be built within the Divi library using the page builder. You can then use these settings to include layouts at certain stages in the page globally. for instance if you wanted a consistent header on every page.. an image perhaps. then you would create a layout in the library and choose it by name here. This plugin will do the rest!</p><p>More information including an introductory video can be seen on the <a href="docs.tortoise-it.co.uk/divi-layout-injector/" target="_blank">documentation website frm Tortoise IT</a>.</p><p>The following image shows the "injection points" currently possible using the plugin.</p>';
        
        echo sb_divi_fe_box_end();
        
        if ($layouts) {
            
            echo '<div style="clear: both;">';
            
            echo sb_divi_fe_box_start('Pre-Header Layout', '49%');
            
            echo '<p><small>This is global unless specified below. In order to use this the <span style="color: red;">fixed navigation must be turned off</span> within the <a href="' . admin_url('admin.php?page=et_divi_options') . '" target="_blank">Divi settings</a>.</small></p>';
            echo '<select style="width: 250px;" name="sb_divi_fe_pre-header">';
            
                $pre_header_layout = get_option('sb_divi_fe_pre-header');
                echo '<option value="">-- None --</option>';
                
                foreach ($layouts as $layout) {
                    echo '<option ' . selected($layout->ID, $pre_header_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                }
    
            echo '</select>';
            
            echo '<p>Applicable on (<a onclick="jQuery(\'.applicable_on_pre-header\').slideToggle();" style="cursor: pointer;">toggle settings</a>)</p>';
            echo '<div class="applicable_on_pre-header" style="display: none;">';
            
            echo '<p><small>This controls where the layout will show. Leave these boxes blank to show everywhere</small></p>';
            
            echo '<p>';
            
            $selected = get_option('sb_divi_fe_applicable_pre-header', array());
            
            foreach ($options as $key=>$value) {
                echo '<label><input type="checkbox" name="sb_divi_fe_applicable_pre-header[' . $key . ']" ' . checked(1, (@$selected[$key] == 1), false) . ' value="1" /> ' . $value . '</label><br />';
            }
            echo '</p>';
            echo '</div>';
            
            //////////////////////////////////////////////////////////////////
            
            if ($languages) {
                
                echo '<p>Polylang applicable on (<a onclick="jQuery(\'.lang_pre-header\').slideToggle();" style="cursor: pointer;">toggle settings</a>)</p>';
                
                echo '<div class="lang_pre-header" style="display: none;">';
                echo '<table style="width: 100%;" class="widefat">';
                    
                foreach ($languages as $language) {
                    if ($default_lang != $language->slug) {
                        $key = 'sb_divi_fe_lang[' . $language->slug . '][pre-header]';
                        
                        echo '<tr><td>' . $language->name . '</td><td>';
                        echo '<select style="width: 250px;" name="' . $key . '">';
                
                            $pre_header_layout = $dli_layout[$language->slug]['pre-header'];
                            echo '<option value="">-- None --</option>';
                            
                            foreach ($layouts as $layout) {
                                echo '<option ' . selected($layout->ID, $pre_header_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                            }
                
                        echo '</select>';
                        echo '</td></tr>';
                    }
                }
                
                echo '</table>';
                echo '</div>';
            }
            
            //////////////////////////////////////////////////////////////////
            
            echo sb_divi_fe_box_end();
            echo sb_divi_fe_box_start('Pre-Content Layout', '49%', 'right');
            
            echo '<p><small>This is global unless specified below</small></p>';
            echo '<select style="width: 250px;" name="sb_divi_fe_pre-content">';
            
                $pre_footer_layout = get_option('sb_divi_fe_pre-content');
                echo '<option value="">-- None --</option>';
                
                foreach ($layouts as $layout) {
                    echo '<option ' . selected($layout->ID, $pre_footer_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                }
    
            echo '</select>';
            
            echo '<p>Applicable on (<a onclick="jQuery(\'.applicable_on_pre-content\').slideToggle();" style="cursor: pointer;">toggle settings</a>)</p>';
            echo '<div class="applicable_on_pre-content" style="display: none;">';
            echo '<p><small>This controls where the layout will show. Leave these boxes blank to show everywhere</small></p>';
            
            echo '<p>';
            
            $selected = get_option('sb_divi_fe_applicable_pre-content', array());
            
            foreach ($options as $key=>$value) {
                echo '<label><input type="checkbox" name="sb_divi_fe_applicable_pre-content[' . $key . ']" ' . checked(1, (@$selected[$key] == 1), false) . ' value="1" /> ' . $value . '</label><br />';
            }
            echo '</p>';
            echo '</div>';
            
            //////////////////////////////////////////////////////////////////
            
            if ($languages) {
                
                echo '<p>Polylang applicable on (<a onclick="jQuery(\'.lang_pre-content\').slideToggle();" style="cursor: pointer;">toggle settings</a>)</p>';
                
                echo '<div class="lang_pre-content" style="display: none;">';
                echo '<table style="width: 100%;" class="widefat">';
                    
                foreach ($languages as $language) {
                    if ($default_lang != $language->slug) {
                        $key = 'sb_divi_fe_lang[' . $language->slug . '][pre-content]';
                        
                        echo '<tr><td>' . $language->name . '</td><td>';
                        echo '<select style="width: 250px;" name="' . $key . '">';
                
                            $pre_content_layout = $dli_layout[$language->slug]['pre-content'];
                            echo '<option value="">-- None --</option>';
                            
                            foreach ($layouts as $layout) {
                                echo '<option ' . selected($layout->ID, $pre_content_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                            }
                
                        echo '</select>';
                        echo '</td></tr>';
                    }
                }
                
                echo '</table>';
                echo '</div>';
            }
            
            //////////////////////////////////////////////////////////////////            
            
            echo sb_divi_fe_box_end();
												
												//post-menu
												echo sb_divi_fe_box_start('Post-Menu Layout', '49%', 'right');
            
            echo '<p><small>This is global unless specified below</small></p>';
            echo '<select style="width: 250px;" name="sb_divi_fe_post-menu">';
            
                $post_menu_layout = get_option('sb_divi_fe_post-menu');
                echo '<option value="">-- None --</option>';
                
                foreach ($layouts as $layout) {
                    echo '<option ' . selected($layout->ID, $post_menu_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                }
    
            echo '</select>';
												
												echo '<p><label><input type="checkbox" name="sb_divi_fe_post-menu_hide_scroll" value="1" ' . checked(get_option('sb_divi_fe_post-menu_hide_scroll'), 1, false) . ' /> Hide Section on Scroll</label></p>';
            
            echo '<p>Applicable on (<a onclick="jQuery(\'.applicable_on_post-menu\').slideToggle();" style="cursor: pointer;">toggle settings</a>)</p>';
            echo '<div class="applicable_on_post-menu" style="display: none;">';
            echo '<p><small>This controls where the layout will show. Leave these boxes blank to show everywhere</small></p>';
            
            echo '<p>';
            
            $selected = get_option('sb_divi_fe_applicable_post-menu', array());
            
            foreach ($options as $key=>$value) {
                echo '<label><input type="checkbox" name="sb_divi_fe_applicable_post-menu[' . $key . ']" ' . checked(1, (@$selected[$key] == 1), false) . ' value="1" /> ' . $value . '</label><br />';
            }
            echo '</p>';
            echo '</div>';
            
            //////////////////////////////////////////////////////////////////
            
            if ($languages) {
                
                echo '<p>Polylang applicable on (<a onclick="jQuery(\'.lang_post-menu\').slideToggle();" style="cursor: pointer;">toggle settings</a>)</p>';
                
                echo '<div class="lang_post-menu" style="display: none;">';
                echo '<table style="width: 100%;" class="widefat">';
                    
                foreach ($languages as $language) {
                    if ($default_lang != $language->slug) {
                        $key = 'sb_divi_fe_lang[' . $language->slug . '][post-menu]';
                        
                        echo '<tr><td>' . $language->name . '</td><td>';
                        echo '<select style="width: 250px;" name="' . $key . '">';
                
                            $pre_content_layout = $dli_layout[$language->slug]['post-menu'];
                            echo '<option value="">-- None --</option>';
                            
                            foreach ($layouts as $layout) {
                                echo '<option ' . selected($layout->ID, $pre_content_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                            }
                
                        echo '</select>';
                        echo '</td></tr>';
                    }
                }
                
                echo '</table>';
                echo '</div>';
            }
            
            //////////////////////////////////////////////////////////////////            
            
            echo sb_divi_fe_box_end();
												//end post menu
            
            echo '</div>';
            echo '<div style="clear: both;">';
            
            echo sb_divi_fe_box_start('Post-Content Layout', '49%');

            echo '<p><small>This is global unless specified below</small></p>';
            echo '<select style="width: 250px;" name="sb_divi_fe_post-content">';
            
                $post_content_layout = get_option('sb_divi_fe_post-content');
                echo '<option value="">-- None --</option>';
                
                foreach ($layouts as $layout) {
                    echo '<option ' . selected($layout->ID, $post_content_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                }
            
            echo '</select>';
            
            echo '<p>Applicable on (<a onclick="jQuery(\'.applicable_on_post-content\').slideToggle();" style="cursor: pointer;">toggle settings</a>)</p>';
            echo '<div class="applicable_on_post-content" style="display: none;">';
            echo '<p><small>This controls where the layout will show. Leave these boxes blank to show everywhere</small></p>';
            
            echo '<p>';
            
            $selected = get_option('sb_divi_fe_applicable_post-content', array());
            
            foreach ($options as $key=>$value) {
                echo '<label><input type="checkbox" name="sb_divi_fe_applicable_post-content[' . $key . ']" ' . checked(1, (@$selected[$key] == 1), false) . ' value="1" /> ' . $value . '</label><br />';
            }
            echo '</p>';            
            echo '</div>';
            
            //////////////////////////////////////////////////////////////////
            
            if ($languages) {
                
                echo '<p>Polylang applicable on (<a onclick="jQuery(\'.lang_post-content\').slideToggle();" style="cursor: pointer;">toggle settings</a>)</p>';
                
                echo '<div class="lang_post-content" style="display: none;">';
                echo '<table style="width: 100%;" class="widefat">';
                    
                foreach ($languages as $language) {
                    if ($default_lang != $language->slug) {
                        $key = 'sb_divi_fe_lang[' . $language->slug . '][post-content]';
                        
                        echo '<tr><td>' . $language->name . '</td><td>';
                        echo '<select style="width: 250px;" name="' . $key . '">';
                
                            $post_content_layout = $dli_layout[$language->slug]['post-content'];
                            echo '<option value="">-- None --</option>';
                            
                            foreach ($layouts as $layout) {
                                echo '<option ' . selected($layout->ID, $post_content_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                            }
                
                        echo '</select>';
                        echo '</td></tr>';
                    }
                }
                
                echo '</table>';
                echo '</div>';
            }
            
            //////////////////////////////////////////////////////////////////            
            
            echo sb_divi_fe_box_end();
            echo sb_divi_fe_box_start('Pre-Footer Layout', '49%', 'right');
            
            echo '<p><small>This is global.. adding it here will make it show on every page</small></p>';
            echo '<select style="width: 250px;" name="sb_divi_fe_pre-footer">';
            
                $pre_footer_layout = get_option('sb_divi_fe_pre-footer');
                echo '<option value="">-- None --</option>';
                
                foreach ($layouts as $layout) {
                    echo '<option ' . selected($layout->ID, $pre_footer_layout, true) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                }
    
            echo '</select>';
            
            //////////////////////////////////////////////////////////////////
            
            if ($languages) {
                
                echo '<p>Polylang applicable on (<a onclick="jQuery(\'.lang_pre-footer\').slideToggle();" style="cursor: pointer;">toggle settings</a>)</p>';
                
                echo '<div class="lang_pre-footer" style="display: none;">';
                echo '<table style="width: 100%;" class="widefat">';
                    
                foreach ($languages as $language) {
                    if ($default_lang != $language->slug) {
                        $key = 'sb_divi_fe_lang[' . $language->slug . '][pre-footer]';
                        
                        echo '<tr><td>' . $language->name . '</td><td>';
                        echo '<select name="' . $key . '">';
                
                            $pre_footer_layout = $dli_layout[$language->slug]['pre-footer'];
                            echo '<option value="">-- None --</option>';
                            
                            foreach ($layouts as $layout) {
                                echo '<option ' . selected($layout->ID, $pre_footer_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                            }
                
                        echo '</select>';
                        echo '</td></tr>';
                    }
                }
                
                echo '</table>';
                echo '</div>';
            }
            
            //////////////////////////////////////////////////////////////////            
            
            echo sb_divi_fe_box_end();
            
            echo '</div>';
            
            echo '<p id="submit" style="clear: both;"><input type="submit" name="sb_divi_fe_edit_submit" class="button-primary" value="Save Settings" /></p>';

        } else {
            echo '<div style="padding:100px; border: 2px solid #999; text-align: center;">
                    <h1>Oops no layouts!</h1>
                    <p style="font-size: 16px;">Please visit the Divi Library to add your first layout and then this page will become available</p>
                    <p><a href="' . (admin_url('/edit.php?post_type=et_pb_layout')) . '" style="display: inline-block; padding: 10px 30px; border: 1px solid #999; font-size: 16px; background-color: #333; color: white; font-weight: bold; border-radius: 20px; text-decoration: none;">Click here to visit the Divi Library</a></p>
                </div>';
        }
        
        echo sb_divi_fe_box_start('Footer');
        
        echo '<p>You can use this shortcode [sb_divi_date] to populate the current year and [sb_divi_blogname] to populate the name of the site. You can also use any other system shortcode to create a message or links of your choice.</p>';
        echo '<p>eg: Copyright [sb_divi_date] [sb_divi_blogname].</p>';
        
        wp_editor( $content, $editor_id );
        
        echo '<p>
            <label><input type="checkbox" name="sb_divi_fe_adv_content" ' . checked(1, get_option('sb_divi_fe_adv_content', 0), false) . ' value="1" /> Enable Advanced Markup</label>
            <br /><small>(The above editor will wrap your content in a HTML paragraph tag for styling purposes. If you wish to add a more complex layout you may need to check this box which will remove the paragraph)</small>
        </p>';

        echo sb_divi_fe_box_end();
        
        echo '<p id="submit"><input type="submit" name="sb_divi_fe_edit_submit" class="button-primary" value="Save Settings" /></p>';
        
        echo '</form>';
        
        echo '</div>';
        echo '</div>';
        
        echo '</div>';
    }
    
class DLI_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'DLI_Widget', // Base ID
            esc_html__( 'Divi Builder Layout', 'text_domain' ), // Name
            array( 'description' => esc_html__( 'Use a layout from the Divi Library/Page Builder.', 'sb_dli' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        echo $args['before_widget'];
		
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }
        
        if ($instance['layout']) {
            echo do_shortcode('[et_pb_section global_module="' . $instance['layout'] . '"][/et_pb_section]');
        }
    
        echo $args['after_widget'];
    }

    public function form( $instance ) {
    		$title = (!empty( $instance['title'] ) ? $instance['title'] : '');
    		$selected_layout = (!empty( $instance['layout'] ) ? $instance['layout'] : 0);
		
        echo '<p>
            <label for="' . esc_attr( $this->get_field_id( 'title' ) ) . '">Title</label> 
            <input class="widefat" id="' . esc_attr( $this->get_field_id( 'title' ) ) . '" name="' . esc_attr( $this->get_field_name( 'title' ) ) . '" type="text" value="' . esc_attr( $title ) . '">
        </p>';
        
        $layout_query = array(
            'post_type'=>'et_pb_layout'
            , 'posts_per_page'=>-1
            , 'meta_query' => array(
                array(
                    'key' => '_et_pb_predefined_layout',
                    'compare' => 'NOT EXISTS',
                ),
            )
        );
        
        if ($layouts = get_posts($layout_query)) {
            echo '<p><label for="' . esc_attr( $this->get_field_id( 'layout' ) ) . '">Layout</label><br />';
            echo '<select style="max-width: 150px;" id="' . esc_attr( $this->get_field_id( 'layout' ) ) . '" name="' . esc_attr( $this->get_field_name( 'layout' ) ) . '">';
            
            foreach ($layouts as $layout) {
                echo '<option  ' . selected($selected_layout, $layout->ID, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
            }
            
            echo '</select></p>';
        } else {
            echo 'No layouts have been added to the Divi Library yet. Add one and this box will populate with a list to choose from.';
        }
    
    }

    public function update( $new_instance, $old_instance ) {
        return $new_instance;
    }

}

?>