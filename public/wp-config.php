<?php

use Dotenv\Dotenv;
require_once(__DIR__ . '/../vendor/autoload.php');
// (new \Dotenv\Dotenv(__DIR__. '/../ '))->load();
$dotenv = Dotenv::create(__DIR__ . '/../');
$dotenv->load();


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv('DB_NAME') );

/** MySQL database username */
define( 'DB_USER', getenv('DB_USER') );

/** MySQL database password */
define( 'DB_PASSWORD', getenv('DB_PASSWORD') );

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@Uvh@r*HzbQ&A*{~,6jA C3SUL|uA&U +M{;R62UM*,-|eS%2H/T$t]yN/JR+=3)');
define('SECURE_AUTH_KEY',  '-1C;CZXC`KxD:6aALz.MC{dy.g$oo%x.^LD{^_X^hm/1QO~kN4$_#)-4nZR{5-yd');
define('LOGGED_IN_KEY',    'EaidY*-auTrrG>mO%F{}d)mhu=V7DKg5LNW}|zKSU+lB:5;KWB67 -s_T(QE&P/y');
define('NONCE_KEY',        'Xuy3yU;H,8|&IReODfk%_GigzQEVJE8h>|E+ZC+fbzM_E|Q)T2yYmWUhV~Q0oIW_');
define('AUTH_SALT',        '+GUw~&p:X>ohU:N7/@M4z,p^#eBn>u-FSa.7-s<6qTgY1~LJ(o]LRm:NQUN^.Q]S');
define('SECURE_AUTH_SALT', 'WO1jmH^1s;B>JW-?s(wu>[t X+|NQu#k;ofhP^5Uk1#|[O(=Oxt==$9-}>tXZRM!');
define('LOGGED_IN_SALT',   'CTxphIAgI;p5l1J>#hf^ses}ba`lS{!D-+h_<f;v?dxfT{N,w~k9T !8g45y|mw ');
define('NONCE_SALT',       'I*-gY_ {+ZIc^@@6u(JCns8t!*b|H&DhAjK0`!xg.fF|MX$!]-<4!8k#>Nf]y#P|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);
@ini_set('display_errors', 0);
define('WP_HOME',getenv('WP_HOME'));
define('WP_SITEURL',getenv('WP_SITEURL'));